import React from "react";
import { Button, Col, Container, Row, Image, Card } from "react-bootstrap";
import "./About.css";
import videoImage from "../../public/assets/img/about-video.jpg";
import client1 from "../../public/assets/img/clients/client-1.png";
import client2 from "../../public/assets/img/clients/client-2.png";
import client3 from "../../public/assets/img/clients/client-3.png";
import client4 from "../../public/assets/img/clients/client-4.png";
import client5 from "../../public/assets/img/clients/client-5.png";
import client6 from "../../public/assets/img/clients/client-6.png";
import testimonials1 from "../../public/assets/img/testimonials/testimonials-1.jpg";
import testimonials2 from "../../public/assets/img/testimonials/testimonials-2.jpg";
import testimonials3 from "../../public/assets/img/testimonials/testimonials-3.jpg";
import testimonials4 from "../../public/assets/img/testimonials/testimonials-4.jpg";
import testimonials5 from "../../public/assets/img/testimonials/testimonials-5.jpg";

import Slider from "react-slick";

function About() {
  const count = [
    {
      count: "65",
      name: "Happy Clients",
    },
    {
      count: "85",
      name: "Projects",
    },
    {
      count: "30",
      name: "Years of experience",
    },
    {
      count: "20",
      name: "Awards",
    },
  ];
  const client = [
    {
      client: client1,
    },
    {
      client: client2,
    },
    {
      client: client3,
    },
    {
      client: client4,
    },
    {
      client: client5,
    },
    {
      client: client6,
    },
  ];

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const testimonial = [
    {
      desc: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.",
      img: testimonials1,
      name: "Saul Goodman",
      title: "Ceo & Founder",
    },
    {
      desc: "Export tempor illum tamen malis malis eram quae irure esselabore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.",
      img: testimonials2,
      name: "Sara Wilsson",
      title: "Designer",
    },
    {
      desc: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.",
      img: testimonials3,
      name: "Jena Karlis",
      title: "Store Owner",
    },
    {
      desc: "Export tempor illum tamen malis malis eram quae irure esselabore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.",
      img: testimonials4,
      name: "Matt Brandon",
      title: "Freelancer",
    },
    {
      desc: "Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.",
      img: testimonials5,
      name: "John Larson",
      title: "Entrepreneur",
    },
  ];

  return (
    <div id="About">
      <Container fluid>
        <div
          style={{ padding: "80px 80px 80px 80px" }}
          className="about-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>ABOUT US</h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem.
              </p>
            </Col>
          </Row>
          <Row
            style={{
              color: "#828282",
              marginTop: "10px",
            }}
          >
            <Col lg={6} md={12}>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
              <ul style={{ paddingLeft: "0px", listStyle: "none" }}>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat
                </li>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Duis aute irure dolor in reprehenderit in voluptate velit
                </li>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat
                </li>
              </ul>
            </Col>
            <Col lg={6} md={12}>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum.
              </p>
              <Button className="button-styles">Learn More</Button>
            </Col>
          </Row>
        </div>
      </Container>
      <Container fluid style={{ backgroundColor: "#F8FBFE" }}>
        <div
          style={{ padding: "0px 80px 0px 80px" }}
          className="about-padding1"
        >
          <Row
            className="justify-content-center text-center"
            style={{
              color: "#124265",
              padding: "50px 0px",
            }}
          >
            {count &&
              count.map((data) => {
                return (
                  <Col lg={3} md={6} sm={6} xs={6}>
                    <strong>
                      <span style={{ fontSize: "48px" }}>{data.count}</span>
                    </strong>
                    <p>{data.name}</p>
                  </Col>
                );
              })}
          </Row>
        </div>
      </Container>
      <Container fluid>
        <div
          style={{ padding: "60px 80px 60px 80px" }}
          className="about-padding"
        >
          <Row>
            <Col
              lg={6}
              md={12}
              style={{ position: "relative" }}
              className="video-part"
            >
              <Image src={videoImage} thumbnail />
              <a
                href="https://www.youtube.com/watch?v=jDDaplaOz7Q"
                style={{ position: "absolute" }}
                className="glightbox video-button"
                data-vbtype="video"
                data-autoplay="true"
              >
                {" "}
              </a>
            </Col>
            <Col lg={6} md={12} style={{ color: "#6F6F6F", fontSize: "15px" }}>
              <h4 style={{ color: "#1E4C6D" }}>
                Voluptatem dignissimos provident quasi corporis voluptates sit
                assumenda.
              </h4>
              <p>
                <i>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </i>
              </p>
              <ul style={{ paddingLeft: "0px", listStyle: "none" }}>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </li>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Duis aute irure dolor in reprehenderit in voluptate velit.
                </li>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Voluptate repellendus pariatur reprehenderit corporis sint.
                </li>
                <li>
                  <i
                    className="fas fa-check-double"
                    style={{ color: "#2889CF", marginRight: "10px" }}
                  ></i>
                  Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                  aute irure dolor in reprehenderit in voluptate trideta
                  storacalaperda mastiro dolore eu fugiat nulla pariatur.
                </li>
              </ul>
              <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum
              </p>
            </Col>
          </Row>
        </div>
      </Container>
      <Container fluid style={{ backgroundColor: "#F8FBFE" }}>
        <div
          className="about-padding2"
          style={{
            color: "#124265",
            padding: "15px 100px",
          }}
        >
          <Row className="justify-content-around ">
            {client &&
              client.map((data) => {
                return (
                  <Col lg={2} md={4} sm={6} xs={6} className="text-center">
                    <Image
                      src={data.client}
                      fluid
                      style={{
                        padding: "15px 0px",
                        maxWidth: "40%",
                        height: "auto",
                      }}
                      className="image"
                    />
                  </Col>
                );
              })}
          </Row>
        </div>
      </Container>
      <Container fluid>
        <div
          style={{ padding: "80px 80px 80px 80px" }}
          className="about-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>
                TESTIMONIALS
              </h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>

          <div
            style={{
              width: "auto",
              display: "block",
              padding: "0px 20px",
            }}
            className="slider-div"
          >
            <Slider {...settings}>
              {testimonial &&
                testimonial.map((data) => {
                  return (
                    <div>
                      <Card
                        style={{ margin: "0px 10px", border: "none" }}
                        className="text-center slider-card"
                      >
                        <Card.Body
                          style={{
                            backgroundColor: "#F3F9FD",
                            borderRadius: "5px",
                            paddingTop: "5px",
                          }}
                        >
                          <Card.Text>
                            <i>{data.desc}</i>
                          </Card.Text>
                        </Card.Body>
                        <Card.Body>
                          <Card.Img
                            src={data.img}
                            className="testimonial-image"
                          />
                          <Card.Title>
                            <p style={{ fontSize: "18px" }}>
                              <strong>{data.name}</strong>
                            </p>
                          </Card.Title>
                          <Card.Subtitle>
                            <p
                              style={{
                                fontSize: "14px",
                                color: "#919191",
                              }}
                            >
                              {data.title}
                            </p>
                          </Card.Subtitle>
                        </Card.Body>
                      </Card>
                    </div>
                  );
                })}
            </Slider>
          </div>
        </div>
      </Container>
    </div>
  );
}

export default About;
