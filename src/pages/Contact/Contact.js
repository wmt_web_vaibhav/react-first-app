import React from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import "./Contact.css";
import { Map, GoogleApiWrapper } from "google-maps-react";

const mapStyles = {
  width: "100%",
  height: "100%",
};

function Contact(props) {
  return (
    <div id="Contact">
      <Container fluid style={{ backgroundColor: "#F9FAFA" }}>
        <div
          style={{ padding: "80px 80px 40px 80px" }}
          className="contact-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>CONTACT</h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Row>
            <Col style={{ height: "300px", width: "100%" }}>
              <Map
                google={props.google}
                zoom={14}
                style={mapStyles}
                initialCenter={{
                  lat: -1.2884,
                  lng: 36.8233,
                }}
              ></Map>
            </Col>
          </Row>
          <Row className="pt-5">
            <Col lg={4}>
              <div className="mb-4 contact-detail">
                <i className="fas fa-map-marker-alt contact-icon"></i>
                <h2
                  style={{
                    fontSize: "22px",
                    color: "#124265",
                    marginLeft: "60px",
                  }}
                >
                  Location:
                </h2>
                <p
                  style={{
                    fontSize: "14px",
                    color: "#217bbc",
                    marginLeft: "60px",
                  }}
                >
                  A108 Adam Street, New York, NY 535022
                </p>
              </div>
              <div className="mb-4 contact-detail">
                <i className="far fa-envelope contact-icon"></i>
                <h2
                  style={{
                    fontSize: "22px",
                    color: "#124265",
                    marginLeft: "60px",
                  }}
                >
                  Email:
                </h2>
                <p
                  style={{
                    fontSize: "14px",
                    color: "#217bbc",
                    marginLeft: "60px",
                  }}
                >
                  info@example.com
                </p>
              </div>
              <div className="mb-4 contact-detail">
                <i className="fas fa-mobile-alt contact-icon"></i>
                <h2
                  style={{
                    fontSize: "22px",
                    color: "#124265",
                    marginLeft: "60px",
                  }}
                >
                  Call:
                </h2>
                <p
                  style={{
                    fontSize: "14px",
                    color: "#217bbc",
                    marginLeft: "60px",
                  }}
                >
                  +1 5589 55488 55s
                </p>
              </div>
            </Col>
            <Col lg={8}>
              <div>
                <Form>
                  <div>
                    <Row>
                      <Col sm={12} lg={6} md={6}>
                        <Form.Group className="mb-3">
                          <Form.Control
                            type="text"
                            placeholder="Your Name"
                            style={{ padding: "20px 10px" }}
                          />
                        </Form.Group>
                      </Col>
                      <Col sm={12} lg={6} md={6}>
                        <Form.Group className="mb-3">
                          <Form.Control
                            type="email"
                            placeholder="Your Email"
                            style={{ padding: "20px 10px" }}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                  </div>
                  <Row>
                    <Col sm={12}>
                      <Form.Group
                        className="mb-3 mt-1"
                        controlId="formBasicSubject"
                      >
                        <Form.Control
                          type="text"
                          placeholder="Subject"
                          style={{ padding: "20px 10px" }}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={12}>
                      <Form.Group
                        className="mb-3 mt-1"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Control
                          as="textarea"
                          rows={4}
                          placeholder="Subject"
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="text-center">
                      <Button
                        size="lg"
                        className="header-pedding button-style mt-1"
                        style={{
                          backgroundColor: "#2487CE",
                          fontSize: "15px",
                          padding: "10px 40px",
                        }}
                      >
                        Send Message
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAuidUMd1cfDV-vP8mSgI8aA-IemewZ6ZM",
})(Contact);
