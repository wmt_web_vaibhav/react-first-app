import React from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import "./Team.css";

import team1 from "../../public/assets/img/team/team-1.jpg";
import team2 from "../../public/assets/img/team/team-2.jpg";
import team3 from "../../public/assets/img/team/team-3.jpg";
import team4 from "../../public/assets/img/team/team-4.jpg";

function Team() {
  const team = [
    {
      img: team1,
      name: "Walter White",
      title: "Chief Executive Officer",
    },
    {
      img: team2,
      name: "Sarah Jhonson",
      title: "Product Manager",
    },
    {
      img: team3,
      name: "William Anderson",
      title: "CTO",
    },
    {
      img: team4,
      name: "Amanda Jepson",
      title: "Accountant",
    },
  ];

  return (
    <div id="Team">
      <Container fluid style={{ backgroundColor: "#F4F7FB" }}>
        <div
          style={{ padding: "80px 80px 70px 80px" }}
          className="team-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>TEAM</h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Row
            className="justify-content-center text-center service-row"
            style={{
              color: "#124265",
              padding: "10px 20px",
            }}
          >
            {team &&
              team.map((data) => {
                return (
                  <Col lg={3} md={4} sm={6} xs={12}>
                    <Card
                      style={{
                        border: "none",
                        margin: "0px 12px 20px 12px",
                        backgroundColor: "#fff",
                      }}
                      className="team-card"
                    >
                      <Card.Img src={data.img} />
                      <div className="social">
                        <a href="# " style={{}}>
                          <i className="fab fa-twitter"></i>
                        </a>
                        <a href="# ">
                          <i className="fab fa-facebook-f"></i>
                        </a>
                        <a href="# ">
                          <i className="fab fa-instagram"></i>
                        </a>
                        <a href="# ">
                          <i className="fab fa-linkedin-in"></i>
                        </a>
                      </div>
                      <Card.Title
                        style={{
                          paddingTop: "25px",
                          fontSize: "18px",
                          fontWeight: "bold",
                        }}
                      >
                        {data.name}
                      </Card.Title>
                      <Card.Subtitle
                        style={{
                          fontSize: "14px",
                          color: "#B3B3B3",
                          paddingBottom: "25px",
                        }}
                      >
                        {data.title}
                      </Card.Subtitle>
                    </Card>
                  </Col>
                );
              })}
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Team;
