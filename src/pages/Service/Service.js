import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import "./Service.css";
import service1 from "../../public/assets/img/testimonials/testimonials-1.jpg";
import service2 from "../../public/assets/img/testimonials/testimonials-2.jpg";
import service3 from "../../public/assets/img/testimonials/testimonials-3.jpg";
import service4 from "../../public/assets/img/testimonials/testimonials-4.jpg";
import service5 from "../../public/assets/img/testimonials/testimonials-5.jpg";
import service6 from "../../public/assets/img/testimonials/testimonials-1.jpg";

function Service() {
  const services = [
    {
      img: service1,
      name: "Lorem Ipsum",
      desc: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    },
    {
      img: service2,
      name: "Sed Perspiciatis",
      desc: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore",
    },
    {
      img: service3,
      name: "Magni Dolores",
      desc: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia",
    },
    {
      img: service4,
      name: "Nemo Enim",
      desc: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis",
    },
    {
      img: service5,
      name: "Dele Cardo",
      desc: "Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur",
    },
    {
      img: service6,
      name: "Divera Don",
      desc: "Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur",
    },
  ];

  return (
    <div id="Service">
      <Container fluid style={{ backgroundColor: "#F4F7FB" }}>
        <div
          style={{ padding: "80px 80px 50px 80px" }}
          className="service-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>SEVICES</h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Row
            className="justify-content-center text-center service-row"
            style={{
              color: "#124265",
              padding: "10px 20px",
            }}
          >
            {services &&
              services.map((data) => {
                return (
                  <Col lg={4} md={6} sm={12} xs={12}>
                    <Card
                      style={{
                        border: "none",
                        marginBottom: "30px",
                        backgroundColor: "#fff",
                      }}
                      className="service-card"
                    >
                      <Card.Body style={{ padding: "70px 20px 80px 20px" }}>
                        <Card.Img src={data.img} className="service-image" />
                        <h5 style={{ paddingTop: "5px", fontWeight: "bold" }}>
                          {data.name}
                        </h5>
                        <p
                          style={{
                            fontSize: "14px",
                            paddingTop: "5px",
                            color: "#535353",
                          }}
                        >
                          {data.desc}
                        </p>
                      </Card.Body>
                    </Card>
                  </Col>
                );
              })}
          </Row>
        </div>
      </Container>
      <Container fluid style={{ backgroundColor: "#2487CE" }}>
        <div
          style={{ padding: "60px 80px 60px 80px" }}
          className="service-padding1"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#fff", fontWeight: "bold" }}>
                Call To Action
              </h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p style={{ fontSize: "16px", color: "#fff" }}>
                Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum.
              </p>
            </Col>
          </Row>
          <Row>
            <Col className="text-center">
              <Button
                size="lg"
                className="service-button-style mt-2"
                style={{ fontWeight: "bold" }}
              >
                Call To Action
              </Button>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Service;
