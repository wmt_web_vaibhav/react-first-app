import React from "react";
import { Button, Row, Col, Container } from "react-bootstrap";
import CardComponent from "./CardComponent";
import "./Home.css";

function Home() {
  const data = [
    {
      title: "A",
      link: "Lorem Ipsum",
      text: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    },
    {
      title: "B",
      link: "Lorem Ipsum",
      text: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    },
    {
      title: "C",
      link: "Lorem Ipsum",
      text: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    },
    {
      title: "D",
      link: "Lorem Ipsum",
      text: "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi",
    },
  ];
  return (
    <div className="bg-image" id="Home">
      <Container fluid className="justify-content-center">
        <div
          style={{ padding: "150px 90px 70px 90px" }}
          className="home-padding"
        >
          <Row className="justify-content-center">
            <Col xl={5} lg={5} className="text-center">
              <h1>One Page Bootstrap Website Template</h1>
              <p>We are team of talented designers</p>
            </Col>
          </Row>
          <Row>
            <Col className="text-center">
              <Button
                size="lg"
                className="header-pedding button-style mt-4"
                style={{
                  backgroundColor: "#2487CE",
                  fontSize: "15px",
                  padding: "10px 40px",
                }}
              >
                Get Started
              </Button>
            </Col>
          </Row>
          <Row
            style={{ marginTop: "100px" }}
            className="justify-content-center"
          >
            {data &&
              data.map((cardData) => {
                return (
                  <Col lg={3} md={6} sm={12}>
                    <CardComponent data={cardData} />
                  </Col>
                );
              })}
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Home;
