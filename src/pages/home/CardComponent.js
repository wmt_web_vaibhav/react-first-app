import React from "react";
import { Card } from "react-bootstrap";

function CardComponent(props) {
  return (
    <div>
      <Card className="shadow-card" border="light" style={{ margin: "10px" }}>
        <Card.Body>
          <Card.Title style={{ color: "#2487ce" }}>
            {props?.data?.title}
          </Card.Title>
          <Card.Link className="card-link" href="#">
            {props?.data?.link}
          </Card.Link>
          <Card.Text className="card-text">{props?.data?.text}</Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

export default CardComponent;
