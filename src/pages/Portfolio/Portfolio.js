import React from "react";
import {
  Container,
  Row,
  Col,
  Tab,
  Nav,
  CardColumns,
  Card,
} from "react-bootstrap";

import portfolio1 from "../../public/assets/img/portfolio/portfolio-1.jpg";
import portfolio2 from "../../public/assets/img/portfolio/portfolio-2.jpg";
import portfolio3 from "../../public/assets/img/portfolio/portfolio-3.jpg";
import portfolio4 from "../../public/assets/img/portfolio/portfolio-4.jpg";
import portfolio5 from "../../public/assets/img/portfolio/portfolio-5.jpg";
import portfolio6 from "../../public/assets/img/portfolio/portfolio-6.jpg";
import portfolio7 from "../../public/assets/img/portfolio/portfolio-7.jpg";
import portfolio8 from "../../public/assets/img/portfolio/portfolio-8.jpg";
import portfolio9 from "../../public/assets/img/portfolio/portfolio-9.jpg";

function Portfolio() {
  const portfolios = [
    {
      img: portfolio1,
      key: "app",
    },
    {
      img: portfolio2,
      key: "web",
    },
    {
      img: portfolio3,
      key: "app",
    },
    {
      img: portfolio4,
      key: "card",
    },
    {
      img: portfolio5,
      key: "web",
    },
    {
      img: portfolio6,
      key: "app",
    },
    {
      img: portfolio7,
      key: "card",
    },
    {
      img: portfolio8,
      key: "card",
    },
    {
      img: portfolio9,
      key: "web",
    },
  ];

  return (
    <div id="Portfolio">
      <Container fluid>
        <div
          style={{ padding: "80px 80px 80px 80px" }}
          className="about-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>
                PORTFOLIO
              </h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Tab.Container defaultActiveKey="all">
            <Nav
              style={{ fontSize: "14px" }}
              className="justify-content-center"
            >
              <Nav.Item>
                <Nav.Link style={{ color: "black" }} eventKey="all">
                  ALL
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link style={{ color: "black" }} eventKey="app">
                  APP
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link style={{ color: "black" }} eventKey="card">
                  CARD
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link style={{ color: "black" }} eventKey="web">
                  WEB
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <Row style={{ marginTop: "30px" }}>
              <Tab.Content>
                <Tab.Pane eventKey="all">
                  <CardColumns style={{ padding: "0px 30px" }}>
                    {portfolios &&
                      portfolios.map((data) => {
                        return (
                          <Card style={{ border: "none" }}>
                            <Card.Img src={data.img} />
                          </Card>
                        );
                      })}
                  </CardColumns>
                </Tab.Pane>
                <Tab.Pane eventKey="app">
                  <CardColumns style={{ padding: "0px 30px" }}>
                    {portfolios &&
                      portfolios.map((data) => {
                        return (
                          data.key === "app" && (
                            <Card style={{ border: "none" }}>
                              <Card.Img src={data.img} />
                            </Card>
                          )
                        );
                      })}
                  </CardColumns>
                </Tab.Pane>
                <Tab.Pane eventKey="card">
                  <CardColumns style={{ padding: "0px 30px" }}>
                    {portfolios &&
                      portfolios.map((data) => {
                        return (
                          data.key === "card" && (
                            <Card style={{ border: "none" }}>
                              <Card.Img src={data.img} />
                            </Card>
                          )
                        );
                      })}
                  </CardColumns>
                </Tab.Pane>
                <Tab.Pane eventKey="web">
                  <CardColumns style={{ padding: "0px 30px" }}>
                    {portfolios &&
                      portfolios.map((data) => {
                        return (
                          data.key === "web" && (
                            <Card style={{ border: "none" }}>
                              <Card.Img
                                src={data.img}
                                className="portfolio-image"
                              />
                            </Card>
                          )
                        );
                      })}
                  </CardColumns>
                </Tab.Pane>
              </Tab.Content>
            </Row>
          </Tab.Container>
        </div>
      </Container>
    </div>
  );
}

export default Portfolio;
