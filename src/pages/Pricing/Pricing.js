import React, { useState } from "react";
import { Container, Row, Col, Card, Button, Collapse } from "react-bootstrap";
import "./Pricing.css";

function Pricing() {
  const [open, setOpen] = useState(null);

  const faqOpen = ({ Id }) => {
    open === Id ? setOpen(null) : setOpen(Id);
  };

  const faq = [
    {
      id: 1,
      que: "Non consectetur a erat nam at lectus urna duis?",
      ans: "Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.",
    },
    {
      id: 2,
      que: "Feugiat scelerisque varius morbi enim nunc?",
      ans: "Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.",
    },
    {
      id: 3,
      que: "Dolor sit amet consectetur adipiscing elit?",
      ans: "Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis",
    },
    {
      id: 4,
      que: "Tempus quam pellentesque nec nam aliquam sem et tortor consequat?",
      ans: "Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.",
    },
    {
      id: 5,
      que: "Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor?",
      ans: "Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.",
    },
  ];

  return (
    <div id="Pricing">
      <Container fluid style={{ backgroundColor: "#F9FAFA" }}>
        <div
          style={{ padding: "80px 80px 40px 80px" }}
          className="pricing-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>PRICING</h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Row
            className="justify-content-center text-center"
            style={{
              color: "#124265",
              padding: "10px 20px",
            }}
          >
            <Col lg={4} md={4} sm={6} xs={12} className="pb-2">
              <Card
                style={{
                  border: "none",
                  margin: "0px 12px 20px 12px",
                  padding: "40px 0px",
                  backgroundColor: "white",
                  color: "#416884",
                  borderRadius: "2%",
                }}
              >
                <Card.Title
                  style={{
                    paddingTop: "15px",
                    fontSize: "18px",
                    fontWeight: "bold",
                    paddingBottom: "12px",
                  }}
                >
                  FREE
                </Card.Title>
                <Card.Title>
                  <h4 style={{ fontSize: "42px", color: "#2487CE" }}>
                    <sup style={{ fontSize: "20px" }}>$ </sup>0
                    <span style={{ fontSize: "16px", color: "#D3D3D3" }}>
                      / month
                    </span>
                  </h4>
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Aida dere
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nec feugiat nisl
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nulla at volutpat dola
                </Card.Title>
                <Card.Title
                  style={{
                    fontSize: "14px",
                    paddingBottom: "12px",
                    textDecoration: "line-through",
                  }}
                >
                  Pharetra massa
                </Card.Title>
                <Card.Title
                  style={{
                    textDecoration: "line-through",
                    fontSize: "14px",
                    paddingBottom: "0px",
                  }}
                >
                  Massa ultricies mi
                </Card.Title>
                <Card.Body>
                  <Button
                    className="pricing-button-style1 mt-2"
                    style={{ fontWeight: "bold" }}
                  >
                    Buy Now
                  </Button>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4} md={4} sm={6} xs={12} className="pb-2">
              <Card
                style={{
                  border: "none",
                  margin: "0px 12px 20px 12px",
                  padding: "40px 0px",
                  backgroundColor: "#2487CE",
                  color: "white",
                  borderRadius: "2%",
                }}
              >
                <Card.Title
                  style={{
                    paddingTop: "15px",
                    fontSize: "18px",
                    fontWeight: "bold",
                    paddingBottom: "12px",
                  }}
                >
                  BUSINESS
                </Card.Title>
                <Card.Title>
                  <h4 style={{ fontSize: "42px" }}>
                    <sup style={{ fontSize: "20px" }}>$ </sup>19
                    <span style={{ fontSize: "16px" }}>/ month</span>
                  </h4>
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Aida dere
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nec feugiat nisl
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nulla at volutpat dola
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Pharetra massa
                </Card.Title>
                <Card.Title
                  style={{
                    textDecoration: "line-through",
                    fontSize: "14px",
                    paddingBottom: "0px",
                  }}
                >
                  Massa ultricies mi
                </Card.Title>
                <Card.Body>
                  <Button
                    className="pricing-button-style2 mt-2"
                    style={{ fontWeight: "bold" }}
                  >
                    Buy Now
                  </Button>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4} md={4} sm={6} xs={12}>
              <Card
                style={{
                  border: "none",
                  margin: "0px 12px 20px 12px",
                  padding: "40px 0px",
                  backgroundColor: "white",
                  color: "#416884",
                  borderRadius: "2%",
                }}
              >
                <Card.Title
                  style={{
                    paddingTop: "15px",
                    fontSize: "18px",
                    fontWeight: "bold",
                    paddingBottom: "12px",
                  }}
                >
                  DEVELOPER
                </Card.Title>
                <Card.Title>
                  <h4 style={{ fontSize: "42px", color: "#2487CE" }}>
                    <sup style={{ fontSize: "20px" }}>$ </sup>29
                    <span style={{ fontSize: "16px", color: "#D3D3D3" }}>
                      / month
                    </span>
                  </h4>
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Aida dere
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nec feugiat nisl
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Nulla at volutpat dola
                </Card.Title>
                <Card.Title style={{ fontSize: "14px", paddingBottom: "12px" }}>
                  Pharetra massa
                </Card.Title>
                <Card.Title
                  style={{
                    fontSize: "14px",
                    paddingBottom: "0px",
                  }}
                >
                  Massa ultricies mi
                </Card.Title>
                <Card.Body>
                  <Button
                    className="pricing-button-style1 mt-2"
                    style={{ fontWeight: "bold" }}
                  >
                    Buy Now
                  </Button>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
      <Container fluid style={{ backgroundColor: "#F4F7FB" }}>
        <div
          style={{ padding: "80px 80px 70px 80px" }}
          className="pricing-padding"
        >
          <Row className="text-center">
            <Col>
              <h2 style={{ color: "#124265", fontWeight: "bold" }}>
                FREQUENTLY ASKED QUESTIONS
              </h2>
            </Col>
          </Row>
          <Row className="text-center">
            <Col>
              <p
                style={{ fontSize: "14px", color: "#969696", marginTop: "8px" }}
              >
                Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                Quia fugiat sit in iste officiis commodi quidem hic quas.
              </p>
            </Col>
          </Row>
          <Row
            className="justify-content-center text-center service-row"
            style={{
              color: "#124265",
              padding: "10px 20px",
            }}
          >
            <Col lg={10}>
              <div className="text-left">
                {faq &&
                  faq.map((data) => {
                    return (
                      <div
                        onClick={() => faqOpen({ Id: data.id })}
                        aria-controls="example-collapse-text"
                        aria-expanded={open === data.id ? true : false}
                        className="faq-question"
                        style={{
                          color: open === data.id ? "#2B8BD0" : "#343A40",
                        }}
                      >
                        <i
                          class="far fa-question-circle"
                          style={{
                            color: "#2889cf",
                            paddingRight: "12px",
                            fontSize: "23px",
                          }}
                        ></i>
                        {data.que}
                        <Collapse in={open === data.id ? true : false}>
                          <div id="example-collapse-text">
                            <p
                              className="pt-2"
                              style={{
                                fontWeight: "normal",
                                marginBottom: "0",
                                color: "#124265",
                              }}
                            >
                              {data.ans}
                            </p>
                          </div>
                        </Collapse>
                      </div>
                    );
                  })}
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Pricing;
