import React from "react";
import { Button, Navbar, Nav, NavDropdown, Container } from "react-bootstrap";
import "./Header.css";

export default function Header(props) {
  return (
    <>
      <Container>
        <Navbar
          fixed="top"
          bg="white"
          expand="lg"
          className="responsive-navbar"
          style={{
            padding: "10px 90px",
            borderBottom: "2px solid #F7FBFD",
          }}
        >
          <Navbar.Brand style={{ color: "#16507B" }}>
            <h2>OnePage</h2>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto" style={{ fontSize: "14px" }}>
              <Nav.Link className="header-pedding" href="#Home">
                Home
              </Nav.Link>
              <Nav.Link className="header-pedding" href="#About">
                About
              </Nav.Link>
              <Nav.Link className="header-pedding" href="#Service">
                Services
              </Nav.Link>
              <Nav.Link className="header-pedding" href="#Portfolio">
                Portfolio
              </Nav.Link>
              <Nav.Link className="header-pedding" href="#Team">
                Team
              </Nav.Link>
              <Nav.Link className="header-pedding" href="#Pricing">
                Pricing
              </Nav.Link>
              <NavDropdown
                className="header-pedding"
                title="Drop Down"
                id="basic-nav-dropdown"
              >
                <NavDropdown.Item href="#">Privacy & Policy</NavDropdown.Item>
                <NavDropdown.Item href="#">Terms & Condition</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#">Contact Us</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link className="header-pedding" href="#Contact">
                Conrtact
              </Nav.Link>
              <Button
                className="header-pedding button-style"
                style={{ backgroundColor: "#2487CE" }}
              >
                Get Started
              </Button>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </>
  );
}
