import React from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import "./Footer.css";

function Footer() {
  return (
    <>
      <div>
        <Container fluid style={{ backgroundColor: "#F9FAFA" }}>
          <div
            style={{ padding: "80px 100px 40px 100px", color: "#777777" }}
            className="footer-padding"
          >
            <Row>
              <Col lg={3} style={{ fontSize: "14px", color: "#777777" }}>
                <h3>One Page</h3>
                <p>
                  A108 Adam Street <br /> New York, NY 535022 <br /> United
                  States
                </p>
                <p>
                  <span style={{ fontWeight: "bold" }}>Phone:</span> +1 5589
                  55488 55
                  <br />
                  <span style={{ fontWeight: "bold" }}>Email: </span>
                  info@example.com
                </p>
              </Col>
              <Col lg={2}>
                <p style={{ color: "#124265" }}>
                  <strong>Useful Links</strong>
                </p>
                <ul
                  style={{
                    listStyle: "none",
                    paddingLeft: "0px",
                    lineHeight: "2",
                  }}
                >
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Home
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      About us
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Services
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Terms of service
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Privacy policy
                    </a>
                  </li>
                </ul>
              </Col>
              <Col lg={3}>
                <p style={{ color: "#124265" }}>
                  <strong>Our Services</strong>
                </p>
                <ul
                  style={{
                    listStyle: "none",
                    paddingLeft: "0px",
                    lineHeight: "2",
                  }}
                >
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Web Design
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Web Development
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Product Management
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Marketing
                    </a>
                  </li>
                  <li>
                    <a
                      href="# "
                      style={{ textDecoration: "none", color: "#777777" }}
                    >
                      Graphic Design
                    </a>
                  </li>
                </ul>
              </Col>
              <Col lg={4} style={{ color: "black" }}>
                <p>
                  <strong>Join Our Newsletter</strong>
                </p>
                <p>
                  Tamen quem nulla quae legam multos aute sint culpa legam
                  noster magna
                </p>
                <Row>
                  <Col lg={12} md={5} sm={5}>
                    <Form.Group className="">
                      <Form.Control
                        type="email"
                        style={{
                          padding: "19px 10px",
                          width: "60%",
                          borderRadius: "5px 0px 0px 5px",
                        }}
                        className="float-left"
                      ></Form.Control>
                      <Button
                        size="lg"
                        className="footer-pedding footer-button-style"
                        style={{
                          backgroundColor: "#2487CE",
                          fontSize: "15px",
                          width: "40%",
                          borderRadius: "0px 5px 5px 0px",
                        }}
                      >
                        Subscribe
                      </Button>
                    </Form.Group>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Container>
        <Container fluid style={{ backgroundColor: "#F4F7FB" }}>
          <div
            style={{ padding: "20px 80px 10px 80px" }}
            className="footer-padding2"
          >
            <Row>
              <Col lg={9}>
                <p style={{ fontSize: "14px" }}>
                  © Copyright <strong>OnePage.</strong> All Rights Reserved
                  <br />
                  <span style={{ fontSize: "13px" }}>
                    Designed by BootstrapMade
                  </span>
                </p>
              </Col>
              <Col lg={3}>
                <Row>
                  <Col>
                    <a href="# " className="footer-icon" style={{}}>
                      <i className="fab fa-twitter"></i>
                    </a>

                    <a href="# " className="footer-icon">
                      <i className="fab fa-facebook-f"></i>
                    </a>

                    <a href="# " className="footer-icon">
                      <i className="fab fa-instagram"></i>
                    </a>

                    <a href="# " className="footer-icon">
                      <i className="fab fa-skype"></i>
                    </a>

                    <a href="# " className="footer-icon">
                      <i className="fab fa-linkedin-in"></i>
                    </a>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    </>
  );
}

export default Footer;
