import React from "react";
import Header from "../Component/Header/Header";
import {} from "react-bootstrap";
import "./layout.css";
import Home from "../pages/Home/Home";
import About from "../pages/About/About";
import Service from "../pages/Service/Service";
import Portfolio from "../pages/Portfolio/Portfolio";
import Team from "../pages/Team/Team";
import Pricing from "../pages/Pricing/Pricing";
import Contact from "../pages/Contact/Contact";
import Footer from "../Component/Footer/Footer";

function layout() {
  return (
    <div>
      <Header />
      <Home />
      <About />
      <Service />
      <Portfolio />
      <Team />
      <Pricing />
      <Contact />
      <Footer />
    </div>
  );
}

export default layout;
